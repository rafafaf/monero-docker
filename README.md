## Presentation

This repository is intended to setup easily a monerod full node.

It is composed of several files :

 * A quick and dirty script to download and verify monero binaries, based on Monero dev's fingerprint : `download-monero.sh`. A storage folder may be specified as a first argument (folder only, no filename). Vars can also be configured at the beginning of script, to download the correct monerod version (OS, arch, type).  
 * A small script that will deploy the downloaded binary at /monero (can be configured in the script) : `deploy-monero-node.sh`  
 * A Dockerfile that will download and deploy monerod automatically. This Dockerfile can be manually build and instancied with `docker build -t monerod ./` then `docker run -d -p18081:18081/tcp -p18080:18080/tcp -v /path/to/blockchain/volume/:/blockchain -it monerod`.  
The Dockerfile can be rebuild using the `--no-cache` option so every step is done again, including the verification for a new version of monerod and IP-blocking file.  
 * A script `build.sh` that build this image automatically with latest monerod version and tag it with the name specified in `.env` file.
 * A `docker-compose.yml` file, for ease of use.
 * a `env.dist` file, that have to be renamed to `.env` and adapted in order to use Compose file and `build.sh` script.


## Usage

Copy end edit the env file :  
`cp env.dist .env && nano .env`

Build the image :  
`./build.sh`

Run the container :  
`docker-compose up -d`
