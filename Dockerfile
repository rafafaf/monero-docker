FROM debian:buster-slim

RUN apt update && apt install -y wget gpg bzip2

COPY ./deploy-monero-node.sh /
COPY ./download-monero.sh /
RUN chmod +x /*monero*.sh
RUN /deploy-monero-node.sh

RUN wget https://gui.xmr.pm/files/block.txt -O /block.txt

EXPOSE 18081 18081/tcp
EXPOSE 18080 18080/tcp

ENTRYPOINT ["/monerod/monerod", "--confirm-external-bind", "--rpc-bind-ip=0.0.0.0", "--restricted-rpc", "--data-dir=/blockchain"]
#ENTRYPOINT ["/monerod/monerod", "--confirm-external-bind", "--rpc-bind-ip=0.0.0.0", "--restricted-rpc", "--data-dir=/blockchain", "--ban-list=block.txt"]
