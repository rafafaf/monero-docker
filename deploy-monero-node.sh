#!/bin/bash

## Vars
monero_install_folder="/monerod/"

# Get monero archive in temporary folder
temp_dir="$(mktemp -d)"
./download-monero.sh -t cli ${temp_dir}

# For cleaning temp folder on exit
function cleanup {
  echo "Removing temp folder"
  rm  -r ${temp_dir}
}
trap cleanup EXIT

cd ${temp_dir}

# Extract archive
tar -xvf monero*
cd monero*/
if [ ! -d $monero_install_folder ]; then
  mkdir $monero_install_folder
fi
mv * ${monero_install_folder}
