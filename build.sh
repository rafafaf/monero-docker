#!/bin/sh

if [ ! -e ./.env ]; then
  echo ".env missing. Copy env.dist to .env and adapt values."
  echo "Exiting"
  exit
fi

. ./.env

docker build -t ${REPO_NAME}/monerod . --no-cache
