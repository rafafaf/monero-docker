#!/bin/bash

## Vars
os="linux" # linux, win, install-win, mac, android, freebsd
arch="x64" # x64, x86, armv7, armv8
type="gui" # cli, gui

# Trusted authors array
# Modify according to monero's development
trusted_devs=( fluffypony binaryfate )

# Hardcoded, because it's the only data i can trust
fluffypony_fingerprint="BDA6 BD70 42B7 21C4 67A9  759D 7455 C5E3 C0CD CEB9"
binaryfate_fingerprint="81AC 591F E9C4 B65C 5806  AFC3 F0AF 4D46 2A0B DF92"


####### Go !!

# Trying to add a help function
function get_help {
  echo "You can specify a target folder where the downloaded archive will be stored.";
  echo "You can specify the type of download with -t option. Accepted values : cli gui"
}

while getopts "ht:" option; do
case "${option}" in
h) get_help;exit;;
t) type="${OPTARG}";;
?) echo "Option -h for info"; exit;;
esac
done

# To remove options as they are parsed, so only arguments are left
shift $(( OPTIND - 1 ))

echo typ : $type

# For cleaning temp folder on exit
function cleanup {
  echo "Removing temp folder"
  rm  -r ${temp_dir}
}
trap cleanup EXIT

## Defining archive storage folder
storage_folder=`pwd`
# if there is an argument
if [ ! -z $1 ]; then
  # and if it's a valid folder
  if [ -d $1 ]; then
    storage_folder=`realpath ${1}`
  else
    echo "Invalid storage folder !"
  fi
fi
echo -e "\nFile will be stored in ${storage_folder}\n"

# Work in temporary folder
temp_dir="$(mktemp -d)"
cd ${temp_dir}

# Adapting filenames for downloads
if [ ${os} = "install-win" ]; then
  type=gui
fi
gui=""
if [ ${type} = "gui" ]; then
  gui="gui-"
fi

## Get and check GPG keys
# For each identified dev
for dev in "${trusted_devs[@]}"; do
  # download GPG key
  wget -q --progress=bar:force -qN https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/${dev}.asc
  # Set fingerprint name for re-use
  fp_name=${dev}_fingerprint
  # Verify if downloaded key matches the fingerprint
  key_OK=`gpg --show-keys --with-fingerprint ${dev}.asc | grep "${!fp_name}"` # ! for expanding fp_name variable
  # If it doesn't match, it's bad. We stop everything so we can understand what's happening
  if [ -z "${key_OK}" ]; then
    echo -e "\n${dev}'s key not OK at all !!!! \nAborting !!\n"
    exit
  else # Everything is fine :)
    echo -e "${dev}'s key totally OK :-)"
    # De-armoring keys for later use
    gpg --dearmor --yes ${dev}.asc
  fi
done

## Get and check hashes.txt file
wget -q --progress=bar:force -qN https://web.getmonero.org/downloads/hashes.txt
# We check the file against any dev's key
for dev in "${trusted_devs[@]}"; do
  gpg --no-default-keyring --keyring ./${dev}.asc.gpg --verify ./hashes.txt 2>/dev/null
  # if return code is 0, it went well
  if [ $? -eq 0 ]; then
    echo -e "\nHashes.txt's signature OK. It has been signed by ${dev}.\n"
    signed_hashes=1
    break
  fi
done
# if no matching signature found
if [ ! ${signed_hashes} ]; then
  echo -e "\nWrong hashes.txt signature !!\n Aborting !!"
  exit
fi

## Extract archive info
# Find the good line in the hashes.txt file
good_line=`cat hashes.txt | grep monero-${gui}${os}-${arch}`
# Find number of fields
fields_count=`echo ${good_line} | wc -w`

# Analyzing every field to find name and version
for i in `seq 1 $fields_count`; do
  # if name or checksum still missing
  if [ -z ${good_filename} ] || [ -z ${good_checksum} ]; then
    # Isolating the field
    block=`echo ${good_line} | awk -v i=$i '{ print $i }' | sed s/,//`
    # Is it the filename ?
    if echo ${block} | grep -q monero-${gui}${os}-${arch} ; then
      good_filename=${block}
      continue
    fi
    # Is it 64 bytes long like a checksum ?
    if [ ${#block} -eq 64 ] ; then
      good_checksum=${block}
      continue
    fi
  fi
done

# If we request a build that doesn't exist
if [ -z ${good_filename}  ]; then
  echo "Bad vars combination ; no such file available !"
  echo "There is no ${type^^} version of monerod for ${os^} on ${arch} processors :-( "
  exit
fi

good_version=`echo ${good_line} | sed -r 's/.*v((\.*[0-9]+)*).*/\1/'`

# Visual recap
echo "Latest version is ${good_version}"
echo "Good filename is ${good_filename}"
echo "Good checksum is ${good_checksum}"
echo ""

# Get the archive file
wget --progress=bar:force -N https://downloads.getmonero.org/${type}/${good_filename} --trust-server-names

# Verify checksum
dl_checksum=`sha256sum ./${good_filename} | awk '{ print $1 }'`
echo "Downloaded : ${dl_checksum}"
echo "Good checksum : ${good_checksum}"
if [ "${dl_checksum}" = "${good_checksum}" ]; then
  echo "Checksum OK - file correctly downloaded !"
  mv ${good_filename} "${storage_folder}"
else # What happened ??
  echo -e "\nBad file ! Baaad fiiiiile !!! \nExit now !! "
  exit
fi
